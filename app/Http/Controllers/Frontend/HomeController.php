<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Artesaos\SEOTools\Facades\SEOTools;


/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */


    public function index()
    {

        return view('frontend.index');
    }

    
}
