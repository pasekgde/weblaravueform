
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


import Vue from 'vue';
import VeeValidate from 'vee-validate';

const VueUploadComponent = require('vue-upload-component')

window.Vue = Vue;
Vue.use(VeeValidate, {
    inject: true,
    fieldsBagName: "veeFields",
    errorBagName: "veeErrors"
});


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('file-upload', VueUploadComponent)

Vue.component('page1-component', require('./components/Page1Component.vue').default);
Vue.component('page2-component', require('./components/Page2Component.vue').default);
Vue.component('page3-component', require('./components/Page3Component.vue').default);
Vue.component('page4-component', require('./components/Page4Component.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    data: {
        isShowComponent1:true,
        isShowComponent2:false,
        isShowComponent3:false,
        isShowComponent4:false,
        currentData:{}

    },
    methods: {
    	 getDataComponent(value){
    	 	this.currentData = value.data
    	 	if(value.visit==='Page1Component'){
    	 		this.reset()
    	 		this.isShowComponent1=true
    	 	}    	 	

    	 	if(value.visit==='Page2Component'){
    	 		this.reset()
    	 		this.isShowComponent2=true
    	 	}
    	 	if(value.visit==='Page3Component'){
    	 		this.reset()
    	 		this.isShowComponent3=true
    	 	}
    	 	if(value.visit==='Page4Component'){
    	 		this.reset()
    	 		this.isShowComponent4=true
    	 	}
    	 },
    	 reset(){
    	 	this.isShowComponent1=false
    	 	this.isShowComponent2=false
    	 	this.isShowComponent3=false
    	 	this.isShowComponent4=false
 
    	 }
               	 
       
    }
});