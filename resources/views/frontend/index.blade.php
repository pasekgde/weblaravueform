@extends('frontend.layouts.app')
@section('content')
    <page1-component v-show="isShowComponent1"  @datacomponent="getDataComponent" :dataprops="currentData"></page1-component>
    <page2-component v-show="isShowComponent2"  @datacomponent="getDataComponent" :dataprops="currentData"></page2-component>
    <page3-component v-show="isShowComponent3"  @datacomponent="getDataComponent" :dataprops="currentData"></page3-component>
    <page4-component v-show="isShowComponent4"  @datacomponent="getDataComponent" :dataprops="currentData"></page4-component>
@endsection

@section('pagespecificscripts')
    {!! script(mix('js/homepage.js')) !!}
@stop